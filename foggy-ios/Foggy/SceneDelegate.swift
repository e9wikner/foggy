//
//  SceneDelegate.swift
//  Foggy
//
//  Created by Stefan Wikner on 2020-03-27.
//  Copyright © 2020 Stefan Wikner. All rights reserved.
//

import UIKit
import SwiftUI
import BackgroundTasks
import CocoaLumberjackSwift

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
        // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
        // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).

        
//        BGTaskScheduler.shared.register(forTaskWithIdentifier: "se.stefanwikner.Foggy.updateIndex", using: nil) { task in
//            // Downcast the parameter to an app refresh task as this identifier is used for a refresh request.
//            self.handleIndexUpdate(task: task as! BGProcessingTask)
//        }
//        BGTaskScheduler.shared.register(forTaskWithIdentifier: "se.stefanwikner.Foggy.syncIndex", using: nil) { task in
//            // Downcast the parameter to an app refresh task as this identifier is used for a refresh request.
//            self.handleIndexSync(task: task as! BGProcessingTask)
//        }
        
        // Use a UIHostingController as window root view controller
//        if let windowScene = scene as? UIWindowScene {
//            let window = UIWindow(windowScene: windowScene)
//            window.rootViewController = UIHostingController(rootView: SyncView())
//            self.window = window
//            window.makeKeyAndVisible()
//        }
    }

    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not neccessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }

    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
        DDLogInfo("Scene: Entering foreground")
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
//        scheduleIndexUpdate()
//        scheduleIndexSync()
    }
    
    
//    // MARK: Background handling
//    
//    
//    func scheduleIndexUpdate() {
//        
//        let request = BGProcessingTaskRequest(identifier: "se.stefanwikner.Foggy.updateIndex")
//        // Fetch no earlier than 15 minutes from now
//        request.earliestBeginDate = Date(timeIntervalSinceNow: 1 * 60)
//        request.requiresNetworkConnectivity = true
//        
//        do {
//            try BGTaskScheduler.shared.submit(request)
//        } catch {
//            DDLogError("Could not schedule app refresh: \(error)")
//        }
//        
//        // in debugger:
//        // e -l objc -- (void)[[BGTaskScheduler sharedScheduler] _simulateLaunchForTaskWithIdentifier:@"se.stefanwikner.Foggy.syncIndex"]
//        
//        DDLogInfo("Scheduled index update")
//    }
//    
//    func handleIndexUpdate(task: BGProcessingTask) {
//        DDLogInfo("Handle index update")
//        
//        scheduleIndexUpdate()
//        
//        let queue = OperationQueue()
//        queue.maxConcurrentOperationCount = 1
//        
//        Index.default.queueUpdates()
//        Index.default.startQueuedUpdates()
//        
//    }
//
//    func scheduleIndexSync() {
//
//        let request = BGProcessingTaskRequest(identifier: "se.stefanwikner.Foggy.syncIndex")
//        // Fetch no earlier than 15 minutes from now
//        request.earliestBeginDate = Date(timeIntervalSinceNow: 2 * 60)
//        request.requiresNetworkConnectivity = true
//
//        do {
//            try BGTaskScheduler.shared.submit(request)
//        } catch {
//            DDLogError("Could not schedule app refresh: \(error)")
//        }
//
//        // in debugger:
//        // e -l objc -- (void)[[BGTaskScheduler sharedScheduler] _simulateLaunchForTaskWithIdentifier:@"se.stefanwikner.Foggy.syncIndex"]
//
//        DDLogInfo("Scheduled index sync")
//    }
//
//    func handleIndexSync(task: BGProcessingTask) {
//        DDLogInfo("Handling index sync")
//
//        scheduleIndexSync()
//
//        let indexSyncManager = IndexSyncManager.default
//        indexSyncManager.sync()
//
//    }

    
}
