//
//  SyncViewModel.swift
//  Foggy
//
//  Created by Stefan Wikner on 2020-11-27.
//  Copyright © 2020 Stefan Wikner. All rights reserved.
//

import Combine
import SwiftUI
import CocoaLumberjackSwift
import Photos
import SQLite
import BackgroundTasks

final class SyncViewModel: ObservableObject, FilesSyncManagerDelegate {
    
    enum SyncState: String {
        
        case launching = "Launching..."
        case launched = "Launched"
        case waitingForAuthorization = "Waiting for Photos autorization..."
        case waitingForConfig = "Waiting for settings configuration..."
        case refreshing = "Checking Photos changes..."
        case refreshed = "Checked Photos changes"
        case connecting = "Connecting to server..."
        case connected = "Connected to server"
        case synchronizing = "Synchronizing..."
        case contentInSync = "Content in sync"
        case contentNeedsSync = "Content needs sync"
        case uploadingContent = "Uploading content to server..."
        case removingContent = "Removing content from server.."
        case error = "Error"
        
        var disableSync: Bool {
            SyncState.contentNeedsSync != self
        }
    }
    
    @ObservedObject var serverManager: ServerManager = ServerManager()
    private var index: Index?
    private var filesSyncManager: FilesSyncManager! = nil
    let idleTimeoutManager = IdleTimeoutManager()
    
    let mainDispatchQueue: DispatchQueueType
    
    private var backgroundTask: BGProcessingTask?
    private var identifiersMissingOnServer = Set<String>()
    private var identifiersToRemoveOnServer = Set<String>()
    
    @Published private(set) var syncProgress: Double = 0.0
    @Published private(set) var syncState: SyncState = .launching
    @Published var syncError: FoggyError?
    @Published var showError = false
    
    private var cancellables = Set<AnyCancellable>()
    private var abortables = Set<AnyCancellable>()
    
    init(mainDispatchQueue: DispatchQueueType = DispatchQueue.main, inhibitLaunch: Bool = false) {
        
        self.mainDispatchQueue = mainDispatchQueue
        addSubscriptions()
        if !inhibitLaunch {
            self.launch()
        }
    }
    
    fileprivate func addSubscriptions() {
        $syncState
            .sink { completion in
                switch completion {
                case .finished:
                    break
                case .failure(let error):
                    self.setErrorState(error as! FoggyError)
                }
            } receiveValue: { [unowned self] newState in
                switchState(newState)
            }
            .store(in: &cancellables)
    }
    
    
    fileprivate func switchState(_ newState: Published<SyncViewModel.SyncState>.Publisher.Output) {
        switch newState {
        case .launched:
            refresh()
        case .refreshed:
            connect()
        case .connected:
            syncIndex()
        case .waitingForAuthorization:
            waitForAuthorization()
        case .contentNeedsSync:
            if backgroundTask != nil {
                syncFiles()
            }
        case .contentInSync, .waitingForConfig, .error:
            finishBackgroundTask(success: true)
        case .launching, .refreshing, .connecting, .synchronizing, .uploadingContent, .removingContent:
            break
        }
    }
    
    private func waitForAuthorization() {
        let seconds = 2.0
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds){ [unowned self] in
            syncState = .launched
        }
    }
    
    private func launch() {
        if index != nil {
            return
        }
        do {
            try index = Index()
        } catch InitError.index {
            self.setErrorState(InitError.index)
        } catch {
            self.setErrorState(InitError.unknown)
        }
        mainDispatchQueue.async { self.syncState = .launched }
    }
    
    
    func refresh() {
        guard let index = index else {
            return
        }
        DDLogInfo("Starting refresh")
        
        mainDispatchQueue.async {
            self.syncState = .refreshing
            self.syncProgress = 0.0
        }
        
        index.refresh()
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { [unowned self] completion in
                switch completion {
                case .failure(let authorizationError):
                    if !index.isAuthorized {
                        syncState = .waitingForAuthorization
                    } else {
                        setErrorState(authorizationError)
                    }
                case .finished:
                    break
                }
            }, receiveValue: { [weak self] value in
                // TOOO: make something out of the returned value
                if value >= 0 {
                    self?.syncState = .refreshed
                }
            })
            .store(in: &abortables)
    }
    
    
    func connect() {
        mainDispatchQueue.async{
            self.syncState = .connecting
        }
        serverManager.sayHello()
            .receive(on: DispatchQueue.main)
            .retry(2)
            .sink(
                receiveCompletion: { [unowned self] value in
                    switch value {
                    case .failure(let helloError):
                        switch helloError {
                        case .config(_):
                            syncState = .waitingForConfig
                        case .requestError(_), .registrationFailed(_), .notRegisteredDevice(_), .unknown(_):
                            setErrorState(helloError)
                        }
                    case .finished:
                        break
                    }
                    
                },receiveValue:  { [weak self] v in
                    self?.syncState = .connected
                })
            .store(in: &abortables)
    }
    
    func syncIndex() {
        guard let index = index else {
            // TODO: error?
            DDLogError("Index is not initialized")
            return
        }
        DDLogInfo("Start index sync")
        mainDispatchQueue.async {
            self.syncState = .synchronizing
        }
        let indexSyncManager = IndexSyncManager()
        
        indexSyncManager.sync(index: index, serverManager: serverManager)
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: {[weak self] value in
                switch value {
                case .failure(let error):
                    self?.setErrorState(error)
                case .finished:
                    break
                }
            }, receiveValue: { [weak self] value in
                
                self?.identifiersMissingOnServer = value.missing
                self?.identifiersToRemoveOnServer = value.remove
                
                let inSync = value.missing.isEmpty && value.remove.isEmpty
                self?.syncState = inSync ? .contentInSync : .contentNeedsSync
            })
            .store(in: &abortables)
        
    }
    
    
    func syncFiles() {
        guard let index = index else {
            return
        }
        
        DDLogInfo("Start uploading \(identifiersMissingOnServer.count) " +
                    "and removing \(identifiersToRemoveOnServer.count) files")
        
        idleTimeoutManager.startManageOfIdleTimeout()
        
        filesSyncManager = FilesSyncManager(index: index, server: serverManager)
        filesSyncManager.delegate = self
        
        filesSyncManager.prepareUpload(identifiersMissingOnServer)
        mainDispatchQueue.async{
            self.syncState = .uploadingContent
        }
        
        didUpload()  // Dummy call to get the upload going
    }
    
    func abort() {
        DDLogInfo("Aborting current tasks")
        abortables.removeAll()
    }
}

extension SyncViewModel {
    func didUpload() {
        
        guard let filesSyncManager = filesSyncManager else {
            return
        }

        let totalNrOfUploads = identifiersMissingOnServer.count
        let remainingNrOfUploads = filesSyncManager.identifiersPendingUpload.count
        mainDispatchQueue.async {
            self.syncProgress = Double(totalNrOfUploads-remainingNrOfUploads)/Double(totalNrOfUploads)
        }
        
        if !filesSyncManager.uploadNext() {
            
            filesSyncManager.prepareRemove(identifiersToRemoveOnServer)
            mainDispatchQueue.async{
                self.syncState = .removingContent
            }
            
            didRemove()  // Dummy call to get the upload going
        }
        
    }
    
    fileprivate func setErrorState(_ error: FoggyError) {
        mainDispatchQueue.async{
            self.syncState = .error
            self.syncError = error
            self.showError = true
        }
        idleTimeoutManager.stopManageOfIdleTimeout()
    }
    
    func failedUpload(with error: FoggyError) {
        mainDispatchQueue.async {
            DDLogError("Failed upload with \(error)")
            self.setErrorState(error)
        }
    }
    
    func didRemove() {
        guard let filesSyncManager = filesSyncManager else {
            return
        }
        
        let totalNrOfRemoves = identifiersToRemoveOnServer.count
        let remainingNrOfRemoves = filesSyncManager.identifiersPendingRemove.count
        mainDispatchQueue.async{
            self.syncProgress = Double(totalNrOfRemoves-remainingNrOfRemoves)/Double(totalNrOfRemoves)
        }
        
        if !filesSyncManager.removeNext() {
            DDLogInfo("Files sync complete")
            mainDispatchQueue.async{
                self.idleTimeoutManager.stopManageOfIdleTimeout()
            }
            syncIndex()
        }
    }
    
    func failedRemove(with error: FoggyError) {
        DDLogInfo("Failed remove with \(error)")
        self.setErrorState(error)
    }
    
// MARK: - Background task handling
    func scheduleBackgroundRefresh() {
        
        finishBackgroundTask(success: false)
        
        // There is a task regestered in the FoggyApp setup that will call handleIndexSync
        let request = BGProcessingTaskRequest(identifier: "se.stefanwikner.Foggy.syncIndex")
        // Refresh in 8 hours (or later) from now
        request.earliestBeginDate = Date(timeIntervalSinceNow: 8 * 60 * 60)
        request.requiresNetworkConnectivity = true
        request.requiresExternalPower = true
        
        do {
            try BGTaskScheduler.shared.submit(request)
            DDLogInfo("Scheduled a new refresh")
        } catch {
            DDLogError("Could not schedule a refresh: \(error)")
        }
        
        // in debugger:
        // e -l objc -- (void)[[BGTaskScheduler sharedScheduler] _simulateLaunchForTaskWithIdentifier:@"se.stefanwikner.Foggy.syncIndex"]
    }
    
    func handleIndexSync(task: BGProcessingTask) {
        DDLogInfo("Got some background time to make a refresh...")
        
        // TODO: we should clean as soon as a file is uploaded
        FileManager.default.clearTmpDirectory()
        
        task.expirationHandler = { [unowned self] in
            DDLogWarn("Background task has expired!")
            self.finishBackgroundTask(success: false)
            // TODO: cancel all jobs
            // TODO: check for error and maybe show notification to user
        }
        scheduleBackgroundRefresh()
        
        backgroundTask = task
        refresh()
    }
    
    func finishBackgroundTask(success: Bool) {
        if let task = backgroundTask {
            DDLogInfo("Background refresh completed")
            task.setTaskCompleted(success: success)
            backgroundTask = nil
        }
    }
    
}


// MARL - DEBUG
#if DEBUG
extension SyncViewModel {
    
    fileprivate func setTempIndex() {
        let tempUrl = FileManager.default.temporaryDirectory.appendingPathComponent("index_temp.db")
        do {
            try FileManager.default.removeItem(at: tempUrl)
        } catch {
            DDLogError("Could not remove temp file at \(tempUrl)")
        }
        
        do {
            try index = Index(in: tempUrl)
        } catch {
            DDLogError("Could not set temp index")
        }
    }

    func setDebugMode() {
        if CommandLine.arguments.contains("--temp-index") {
            DDLogDebug("Setting temp index")
            setTempIndex()
        }
    }
}
#endif
