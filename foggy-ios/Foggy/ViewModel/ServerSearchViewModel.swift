//
//  ServerSearchViewModel.swift
//  Foggy
//
//  Created by Stefan Wikner on 2021-01-14.
//  Copyright © 2021 Stefan Wikner. All rights reserved.
//

import Foundation
import SwiftUI
import Combine

final class ServerSearchViewModel: ObservableObject {
    
    @AppStorage(K.appStorageKeys.serverIp) var serverAddress : String = ""
    @AppStorage(K.appStorageKeys.serverName) var serverHostName: String = ""
    @Published var searchInProgress : Bool = false
    
    private var cancellables = Set<AnyCancellable>()
    
    func findServerOnLocalNetwork() {
        
        let netServiceProvider = NetServiceProvider()
        netServiceProvider.startSearch()
        
        searchInProgress = true
        
        netServiceProvider.$searchResult
            .receive(on: DispatchQueue.main)
            .compactMap { $0 }  // drop nil
            .handleEvents(receiveCancel: { [weak self] in
                if let searchInProgress = self?.searchInProgress {
                    if searchInProgress {
                        netServiceProvider.abortSearch()
                        self?.searchInProgress = false
                    }
                }
            })
            .handleEvents(receiveOutput: { [weak self] server in
                self?.searchInProgress = false
            })
            .sink { [weak self] server in
                self?.serverAddress = server.ip
                self?.serverHostName = server.hostname
            }
            .store(in: &cancellables)
        
    }
    
    func cancel() {
        self.cancellables.removeAll()
    }
    
}
