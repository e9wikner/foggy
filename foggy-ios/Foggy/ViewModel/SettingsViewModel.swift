//
//  SettingsStore.swift
//  Foggy
//
//  Created by Stefan Wikner on 2020-11-13.
//  Copyright © 2020 Stefan Wikner. All rights reserved.
//

import SwiftUI
import Combine

final class SettingsViewModel: ObservableObject {
    
    @AppStorage(K.appStorageKeys.serverIp) var serverAddress : String = ""
    @AppStorage(K.appStorageKeys.serverName) var serverHostName: String = ""
    @AppStorage(K.appStorageKeys.userName) var userName: String = ""
    
    @Published var password: String = ""
    
    @Published var disableSearchButton: Bool = false
    @Published var disableDoneButton: Bool = false
    
    private var cancellables = Set<AnyCancellable>()
    
    var isEditing: Bool = false {
        didSet {
            disableButtons = isEditing
        }
    }
    
    var disableButtons: Bool = false {
        didSet {
            disableSearchButton = disableButtons
            disableDoneButton = disableButtons
        }
    }
    
    func validateSettings(){
        disableSearchButton = false
        disableDoneButton = false
    }
    
    func register() {
        let server = ServerManager()
        server.registerDevice()
            .mapError({ serverError -> Error in
                HelloError.registrationFailed(description: serverError.localizedDescription) // TODO: registerDevice() should only throw HelloError
            })
            .catch { error -> Just<RegistrationResponse> in
                print(error.localizedDescription)  // TODO
                let emptyResponse = RegistrationResponse(password: "")
                return Just(emptyResponse)
            }
            .map { response in
                return response.password
            }
            .receive(on: DispatchQueue.main)
            .assign(to: &$password)
    }
}

#if DEBUG
extension SettingsViewModel {
    
    fileprivate func clearSettings() {
        serverAddress = ""
        serverHostName = ""
        userName = ""
    }
    
        func setDebugMode() {
        if CommandLine.arguments.contains("--clear-settings") {
            clearSettings()
        }
    }
}
#endif
