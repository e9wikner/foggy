//
//  SyncManager.swift
//  Foggy
//
//  Created by Stefan Wikner on 2020-04-20.
//  Copyright © 2020 Stefan Wikner. All rights reserved.
//
import Combine
import CocoaLumberjackSwift


class IndexSyncManager {
    
    func sync(index: Index, serverManager: ServerManager) -> AnyPublisher<IndexSyncResponse, ServerError> {
        
        guard let url = serverManager.apiUrl(route: "index") else {
            let error = ServerError.config(description: "Cannot sync since no server has been discovered yet")
            DDLogError(error.localizedDescription)
            return Fail(error: error).eraseToAnyPublisher()
        }
        
        let session = URLSession.shared
        var request = URLRequest(url: url)
        request.httpMethod = "PUT"
        
        return session.uploadTaskPublisher(for: request, from: index.url)
            .tryMap() { element -> IndexSyncResponse in
                guard let httpResponse = element.response as? HTTPURLResponse, httpResponse.statusCode == 200 else {
                    throw URLError(.badServerResponse)
                }
                return IndexSyncResponse(from: element.data)
            }
            .mapError { error in
                .network(description: error.localizedDescription)
            }
            .eraseToAnyPublisher()
    }
}
