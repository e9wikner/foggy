//
//  MediaItem.swift
//  Foggy
//
//  Created by Stefan Wikner on 2020-04-03.
//  Copyright © 2020 Stefan Wikner. All rights reserved.
//

import Photos
import Combine
import CocoaLumberjackSwift

protocol FilesSyncManagerDelegate {
    func didUpload()
    func failedUpload(with error: FoggyError)
    func didRemove()
    func failedRemove(with error: FoggyError)
}

class FilesSyncManager {
    
    var delegate: FilesSyncManagerDelegate?
    let serverManager: ServerManager
    let index: Index
    
    // TODO: cancel on this when needed
    var phImageRequestIds: Set<PHImageRequestID> = Set()
    var cancellables: Set<AnyCancellable> = Set()
    
    var identifiersPendingUpload = Set<String>()
    var identifiersPendingRemove = Set<String>()
    
    init(index: Index, server: ServerManager) {
        self.index = index
        self.serverManager = server
    }
    
    func apiUrl(identifier: String) -> URL? {
        if let apiUrl = serverManager.apiUrl(route: "files") {
            return URL(string: "\(apiUrl)/\(identifier)")
        } else {
            return nil
        }
    }
    
    func prepareRemove(_ identifiers: Set<String>) {
        DDLogInfo("Preparing remove of files on remote")
        identifiersPendingRemove = identifiers
    }
    
    func prepareUpload(_ identifiers: Set<String>) {
        DDLogInfo("Preparing upload of files")
        identifiersPendingUpload = identifiers
    }
    
    func uploadNext() -> Bool {
        
        guard let identifier = identifiersPendingUpload.popFirst() else {
            return false
        }
        
        let fetchResult = PHAsset.fetchAssets(withLocalIdentifiers: [identifier], options: nil)
        
        guard let asset = fetchResult.firstObject else {
            DDLogError("Failed to fetch PHAsset for \(identifier)")
            return true  // TODO: throw exception to be handled
        }
        
        // TODO: exporting of large files will eventually crash the app since it is all stored in memory before being uploaded
        
        DDLogInfo("Preparing upload of \(asset.identifier)")
        asset.dataPublisher(index: index)
            .sink(receiveCompletion: { [weak self] completion in
                switch completion {
                case .finished:
                    break
                case .failure(let requestDataError):
                    self?.delegate?.failedUpload(with: requestDataError as FoggyError)
                }
            }, receiveValue: { [weak self] (data) in  // TODO: can we avoid weak self?
                    self?.startUpload(withData: data,
                                      andIdentifier: asset.identifier)  // TODO: store request id to be able to cancel it
            })
            .store(in: &cancellables)
        return true
    }
    
    func removeNext() -> Bool {
        
        guard let identifier = identifiersPendingRemove.popFirst() else {
            return false
        }
        guard let url = apiUrl(identifier: identifier) else {
            return false
        }
        var request = URLRequest(url: url)
        request.httpMethod = "DELETE"
        
        DDLogInfo("Creating remove task: \(url.absoluteString)")
        
        let task = URLSession.shared.downloadTask(with: request) {
            (url_response, response, error) in
            
            if let safeError = error {
                DDLogError(safeError.localizedDescription)
                
                if let urlError = safeError as? URLError {
                    
                    if urlError.code == URLError.timedOut {
                        let serverError = ServerError.network(description: "Timeout")
                        self.delegate?.failedRemove(with: serverError)
                        return
                    }
                } else {
                    let serverError = ServerError.network(description: "Unknown")
                    self.delegate?.failedRemove(with: serverError)
                }
            } else {
                self.delegate?.didRemove()
            }
        }
        
        task.resume()
        return true
    }
    
    private func startUpload(withData uploadData: Data, andIdentifier identifier: String) -> URLSessionTask? {
        
        guard let url = apiUrl(identifier: identifier) else {
            return nil
        }
        var request = URLRequest(url: url)
        request.httpMethod = "PUT"
        
        DDLogInfo("Creating upload task \(identifier) -> \(url.absoluteString)")
        
        let task = URLSession.shared.uploadTask(with: request, from: uploadData) {
            (data, response, error) in
            DDLogInfo("Upload finished for \(identifier)")
            
            if let safeError = error {
                DDLogError(safeError.localizedDescription)
                
                if let urlError = safeError as? URLError {
                    
                    if urlError.code == URLError.timedOut {
                        self.delegate?.failedRemove(with: ServerError.networkTimeout)
                        return
                    }
                } else {
                    let serverError = ServerError.network(description: "Unknown")
                    self.delegate?.failedRemove(with: serverError)
                }
                
            } else {
                self.delegate?.didUpload()
            }
        }
        
        DDLogInfo("Resuming upload")
        task.resume()
        return task
    }
    
    
}

// MARK: - Data hashing

