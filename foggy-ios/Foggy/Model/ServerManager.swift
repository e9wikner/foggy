//
//  Server.swift
//  Foggy
//
//  Created by Stefan Wikner on 2020-04-21.
//  Copyright © 2020 Stefan Wikner. All rights reserved.
//
import Foundation
import CocoaLumberjackSwift
import Combine
import SwiftUI  // TODO: this is not OK, remove when @AppStorage is removed


class ServerManager : ObservableObject {
    
    @AppStorage(K.appStorageKeys.serverIp) internal var ip : String = ""
    @AppStorage(K.appStorageKeys.serverName) internal var name : String = ""
    @AppStorage(K.appStorageKeys.userName) internal var userName: String = ""
    
    @Published var errorMessage : String?
    
    // TODO: check if this can be handled better with throws
    var appUrl : String? {
        if ip != "" {
            return "http://\(ip):21210/\(K.app)"
        } else {
            DDLogError("Empty server ip address")
            return nil
        }
    }
    
    func apiUrl(route : String) -> URL? {
        guard let appUrl = appUrl else {
            DDLogError("Missing server url")
            return nil
        }
        let urlString = "\(appUrl)/\(route)/\(K.deviceId!)"
        return URL(string: urlString)
    }
    
    func registerDevice() -> AnyPublisher<RegistrationResponse, HelloError> {
        
        guard let url = apiUrl(route: "hello") else {
            let error = HelloError.config(
                description: "Cannot register since no server has been discovered yet")
            DDLogError(error.localizedDescription)
            return Fail(error: error).eraseToAnyPublisher()
        }
        
        let registrationData = RegistrationData(user: userName,
                                                device_name: K.deviceName)
        guard let registrationJsonData = try? JSONEncoder().encode(registrationData) else {
            let error = HelloError.config(
                description: "Cannot register with \(registrationData)")
            DDLogError(error.localizedDescription)
            return Fail(error: error).eraseToAnyPublisher()
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "PUT"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = registrationJsonData
        
        DDLogInfo("Registering device with server: \(url.absoluteString)")
        let session = URLSession.shared
        return session.dataTaskPublisher(for: request)
            .tryMap() { element -> Data in
                guard let httpResponse = element.response as? HTTPURLResponse,
                      httpResponse.statusCode == 200 else {
                    throw URLError(.badServerResponse)
                }
                return element.data
            }
            .decode(type: RegistrationResponse.self, decoder: JSONDecoder())
            .mapError { error in
                switch (error) {
                case let urlError as URLError:
                    return .requestError(urlError)
                case let helloError as HelloError:
                    return helloError
                default:
                    return .unknown(description: error.localizedDescription)
                }
            }
            .eraseToAnyPublisher()
    }
    
    func sayHello() -> AnyPublisher<Data, HelloError> {
        
        guard let url = apiUrl(route: "hello") else {
            let error = HelloError.config(
                description: "Cannot say hello since no server has been discovered yet")
            DDLogError(error.localizedDescription)
            return Fail(error: error).eraseToAnyPublisher()
        }
        
        DDLogInfo("Saying hello to server: \(url.absoluteString)")
        
        let session = URLSession.shared
        
        return session.dataTaskPublisher(for: url)
            .tryMap { element -> Data in
                if let httpResponse = element.response as? HTTPURLResponse{
                    switch httpResponse.statusCode {
                    case (200...299):
                        return element.data
                    case (404):
                        throw HelloError.notRegisteredDevice(deviceId: K.deviceId!)
                    default:
                        throw HelloError.unknown(description: "Bad response: \(element.response)")
                    }
                } else {
                    throw ServerError.network(description: "Unknown error")
                }
            }
            .mapError { (error) -> HelloError in
                switch (error) {
                case let urlError as URLError:
                    return .requestError(urlError)
                case let helloError as HelloError:
                    return helloError
                default:
                    return .unknown(description: error.localizedDescription)
                }
            }
            .eraseToAnyPublisher()
    }
}
