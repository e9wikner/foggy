//
//  FileManager+.swift
//  Foggy
//
//  Created by Stefan Wikner on 2021-12-10.
//  Copyright © 2021 Stefan Wikner. All rights reserved.
//

import CocoaLumberjackSwift

extension FileManager {
    func clearTmpDirectory() {
        do {
            let temporaryFiles = try self.contentsOfDirectory(atPath: self.temporaryDirectory.path)
            try temporaryFiles.forEach { file in
                let url = self.temporaryDirectory.appendingPathComponent(file)
                try self.removeItem(at: url)
            }
            DDLogInfo("Cleaned up \(temporaryFiles.count) temporary files")
        } catch {
            DDLogError(error)
        }
    }
}
