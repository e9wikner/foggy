//
//  URLSession+.swift
//  Foggy
//
// inspired by: https://theswiftdev.com/how-to-download-files-with-urlsession-using-combine-publishers-and-subscribers/
// until Apple releases a publisher for upload tasks
//
//  Created by Stefan Wikner on 2020-11-30.
//  Copyright © 2020 Stefan Wikner. All rights reserved.
//

import Foundation
import Combine

extension URLSession {

    public func uploadTaskPublisher(for url: URL, from file: URL) -> URLSession.UploadTaskPublisher {
        self.uploadTaskPublisher(for: .init(url: url), from: file)
    }

    public func uploadTaskPublisher(for request: URLRequest, from file: URL) -> URLSession.UploadTaskPublisher {
        .init(request: request, session: self, file: file)
    }

    public struct UploadTaskPublisher: Publisher {

        public typealias Output = (data: Data, response: URLResponse)
        public typealias Failure = URLError

        public let request: URLRequest
        public let session: URLSession
        public let file: URL

        public init(request: URLRequest, session: URLSession, file: URL) {
            self.request = request
            self.session = session
            self.file = file
        }

        public func receive<S>(subscriber: S) where S: Subscriber,
            UploadTaskPublisher.Failure == S.Failure,
            UploadTaskPublisher.Output == S.Input
        {
            let subscription = UploadTaskSubscription(subscriber: subscriber, session: self.session, request: self.request, file: self.file)
            subscriber.receive(subscription: subscription)
        }
    }
}

extension URLSession {

    final class UploadTaskSubscription<SubscriberType: Subscriber>: Subscription where
        SubscriberType.Input == (data: Data, response: URLResponse),
        SubscriberType.Failure == URLError
    {
        private var subscriber: SubscriberType?
        private weak var session: URLSession!
        private var request: URLRequest!
        private var task: URLSessionUploadTask!
        private var file: URL

        init(subscriber: SubscriberType, session: URLSession, request: URLRequest, file: URL) {
            self.subscriber = subscriber
            self.session = session
            self.request = request
            self.file = file
        }

        func request(_ demand: Subscribers.Demand) {
            guard demand > 0 else {
                return
            }
            task = self.session.uploadTask(with: request, fromFile: file) { [weak self] data, response, error in
                if let error = error as? URLError {
                    self?.subscriber?.receive(completion: .failure(error))
                    return
                }
                guard let response = response else {
                    self?.subscriber?.receive(completion: .failure(URLError(.badServerResponse)))
                    return
                }
                guard let data = data else {
                    self?.subscriber?.receive(completion: .failure(URLError(.badURL)))
                    return
                }
                
                let result = (data: data, response: response)
                self?.subscriber?.receive(result)
                self?.subscriber?.receive(completion: .finished)
            }
            task.resume()
        }

        func cancel() {
            task.cancel()
        }
    }
}


extension URLSession {
    
    func uploadTask(with request: URLRequest, from data: Data) -> Future<(Data?, URLResponse?, Error?),Never> {
        return Future { promise in
            let task = self.uploadTask(with: request, from: data) { (data, response, error) in
                promise(.success((data, response, error)))
            }
            
        }
    }
}
