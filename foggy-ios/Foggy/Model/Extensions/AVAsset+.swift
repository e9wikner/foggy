//
//  AVAssetExportSession.swift
//  Foggy
//
//  Created by Stefan Wikner on 2021-12-09.
//  Copyright © 2021 Stefan Wikner. All rights reserved.
//

import Combine
import Photos

extension AVAsset{
    
    /**
     Publisher for the url to the local file of the AVAsset
     
     If the file can be read directly this func returns immediately. Otherwise it has to start an
     export session that will later return the url to the exported file.
     */
    func urlReadyToBeRead(localIdentifier: String) -> AnyPublisher<URL,ExportPHAssetDataError> {
        if let avUrlAsset = self as? AVURLAsset{
            return Just(avUrlAsset.url)
                .setFailureType(to: ExportPHAssetDataError.self)
                .eraseToAnyPublisher()
        } else {
            let exportUrl = FileManager.default.temporaryDirectory.absoluteURL.appendingPathComponent(localIdentifier)
            return exportData(outputUrl: exportUrl).eraseToAnyPublisher()
        }
    }
    
    private func exportData(outputUrl: URL) -> Future<URL, ExportPHAssetDataError> {
        return Future() { promise in
            let preset = AVAssetExportPresetHighestQuality
            let outFileType: AVFileType = AVFileType.mov
            
            AVAssetExportSession.determineCompatibility(ofExportPreset: preset,
                                                        with: self,
                                                        outputFileType: outFileType
            ) { isCompatible in
                guard isCompatible else {
                    let error = ExportPHAssetDataError.exportSessionFailed
                    return promise(.failure(error))
                }
            }
            
            guard let exportSession = AVAssetExportSession(
                asset: self,
                presetName: preset
            ) else {
                let error = ExportPHAssetDataError.exportSessionFailed
                return promise(.failure(error))
            }
            exportSession.outputURL = outputUrl
            exportSession.outputFileType = outFileType
            exportSession.shouldOptimizeForNetworkUse = true
        
            exportSession.exportAsynchronously {
                promise(.success(outputUrl))
            }
        }
    }
}

