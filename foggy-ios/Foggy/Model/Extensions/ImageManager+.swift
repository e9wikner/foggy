//
//  ImageManager+.swift
//  Foggy
//
//  Created by Stefan Wikner on 2020-12-01.
//  Copyright © 2020 Stefan Wikner. All rights reserved.
//

import Foundation
import Combine
import Photos

extension PHImageManager {
    func requestImageDataAndOrientation(for asset: PHAsset,
                                        options: PHImageRequestOptions?)
    -> Future<(Data?, String?, CGImagePropertyOrientation, [AnyHashable : Any]?),Never> {
        return Future { promise in
            let requestId = self.requestImageDataAndOrientation(for: asset, options: options) { (imageData, dataUniformTypeIdentifier, imageOrientation, info) in
                
                promise(.success((imageData, dataUniformTypeIdentifier, imageOrientation, info)))
            }
        }
    }
    
    func requestAVAsset(forVideo asset: PHAsset,
                        options: PHVideoRequestOptions?)
    -> Future<(AVAsset?, AVAudioMix?, [AnyHashable : Any]?),Never> {
        return Future { promise in
            let requestId = self.requestAVAsset(forVideo: asset, options: options) { (avAsset, avAudioMix, info) in
                promise(.success((avAsset, avAudioMix, info)))
            }
        }
    }
}
