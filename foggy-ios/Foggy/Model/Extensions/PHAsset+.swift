//
//  PHAsset+.swift
//  Foggy
//
//  Created by Stefan Wikner on 2020-12-01.
//  Copyright © 2020 Stefan Wikner. All rights reserved.
//

import Foundation
import Photos
import Combine
import CocoaLumberjackSwift


extension PHAsset {
    var identifier: String {
        localIdentifier.replacingOccurrences(of: "/", with: "-")
    }
    
    var timestampCreated: Double {
        if let createdDate = creationDate {
            let timeInterval = createdDate.timeIntervalSince1970
            return timeInterval as Double
        } else{
            return 0.0
        }
    }
    
    var timestampModified: Double {
        if let modificationDate = modificationDate {
            let timeInterval = modificationDate.timeIntervalSince1970
            return timeInterval as Double
        } else {
            return timestampCreated
        }
    }
    
    fileprivate func checkInfo(_ info: [AnyHashable : Any]) throws {
        if (info[PHImageResultIsInCloudKey] != nil) {
            let description = "TODO: handle PHImageResultIsInCloudKey (\(self.localIdentifier))"
            DDLogError(description)
            throw ExportPHAssetDataError.resultIsInCloud(description: description)
        }
        if ((info[PHImageCancelledKey]) != nil) {
            DDLogInfo("Cancelled export (\(self.localIdentifier))")
        }
        if let errorDescription = info[PHImageErrorKey] {
            let message = "Error requesting \(self.localIdentifier): \(errorDescription)"
            DDLogError(message)
            
            throw ExportPHAssetDataError.imageError(description: message)
        }
    }
    
    func dataPublisher(index: Index) -> AnyPublisher<Data,ExportPHAssetDataError> {
        
        let imageManager = PHImageManager.default()
        
        switch mediaType {
            
        case PHAssetMediaType.image:
            
            let imageRequestOptions = PHImageRequestOptions()
            
            imageRequestOptions.isNetworkAccessAllowed = true
            imageRequestOptions.deliveryMode = .highQualityFormat
            imageRequestOptions.version = .current
            
            // TODO: save request ID to be able to cancel request when needed
            return imageManager.requestImageDataAndOrientation(for: self, options: imageRequestOptions)
                .tryMap { [unowned self] (imageData, dataUniformTypeIdentifier, imageOrientation, info) in
                    
                    if let safeInfo = info {
                        try checkInfo(safeInfo)
                    }
                    
                    if let safeImageData = imageData {
                        
                        switch dataUniformTypeIdentifier! {
                            // See this for UTI values:                                    https://developer.apple.com/library/archive/documentation/Miscellaneous/Reference/UTIRef/Articles/System-DeclaredUniformTypeIdentifiers.html#//apple_ref/doc/uid/TP40009259-SW1
                        case "public.heic", "public.png" :
                            let ciImage = CIImage(data: safeImageData)
                            // TODO: what happens below iOS 10.0
                            // TODO: what happens when image already is stored in jpeg?
                            
                            if #available(iOS 10.0, *) {
                                let jpegData : Data = CIContext()
                                    .jpegRepresentation(of: ciImage!,
                                                        colorSpace: CGColorSpaceCreateDeviceRGB())!
                                index.assetWasExported(asset: self, data: jpegData)
                                return jpegData
                            }
                        case "public.jpeg":
                            
                            index.assetWasExported(asset: self, data: safeImageData)
                            return safeImageData
                            
                        default:
                            
                            throw ExportPHAssetDataError.imageError(
                                description: "Unknown UTI: \(String(describing: dataUniformTypeIdentifier))")
                            
                        }
                    }
                    
                    throw ExportPHAssetDataError.imageError(description: "Export \(self.localIdentifier) failed")
                    
                    
                }
                .mapError { error in
                .imageError(description: error.localizedDescription)
                }
                .eraseToAnyPublisher()
            
        case PHAssetMediaType.video:
            
            let videoRequestOptions = PHVideoRequestOptions()
            
            videoRequestOptions.isNetworkAccessAllowed = true
            videoRequestOptions.deliveryMode = .highQualityFormat
            videoRequestOptions.version = .current
            
            return imageManager.requestAVAsset(forVideo: self, options: videoRequestOptions)
                .setFailureType(to: ExportPHAssetDataError.self)
                .tryMap { [unowned self] avAsset, avAudioMix, info -> AVAsset in
                    if let safeInfo = info {
                        try checkInfo(safeInfo)
                    }
                    if let safeAsset = avAsset {
                        return safeAsset
                    } else {
                        throw ExportPHAssetDataError.couldNotGetAssetUrl
                    }
                }
                .flatMap { avAsset in
                    return avAsset.urlReadyToBeRead(localIdentifier: self.identifier)
                        .mapError { $0 as ExportPHAssetDataError}  // TODO: why need to map this?
                }
                .tryMap{ url -> Data in
                    do {
                        let data = try Data.init(contentsOf: url)
                        index.assetWasExported(asset: self, data: data)
                        return data
                    } catch {
                        throw ExportPHAssetDataError.failedReadingData
                    }
                }
                .mapError { error in
                .imageError(description: error.localizedDescription)
                }
                .eraseToAnyPublisher()
            
        default:
            return Fail(error: ExportPHAssetDataError.imageError(description: "Unsupported media type \(self.mediaType)"))
                .eraseToAnyPublisher()
        }
    }
}
