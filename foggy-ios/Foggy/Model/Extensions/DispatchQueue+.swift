//
//  DispatchQueue+.swift
//  Foggy
//
//  Created by Stefan Wikner on 2021-01-28.
//  Copyright © 2021 Stefan Wikner. All rights reserved.
//

import Foundation

// This is taken from:
// https://stackoverflow.com/questions/53002173/how-to-test-method-that-is-called-with-dispatchqueue-main-async

protocol DispatchQueueType {
    func async(execute work: @escaping @convention(block) () -> Void)
}

extension DispatchQueue: DispatchQueueType {
    func async(execute work: @escaping @convention(block) () -> Void) {
        async(group: nil, qos: .unspecified, flags: [], execute: work)
    }
}
