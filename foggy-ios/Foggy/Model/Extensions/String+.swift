//
//  String+.swift
//  Foggy
//
//  Created by Stefan Wikner on 2021-01-14.
//  Copyright © 2021 Stefan Wikner. All rights reserved.
//

import Foundation

// Source: https://www.hackingwithswift.com/example-code/strings/how-to-convert-a-string-to-a-safe-format-for-url-slugs-and-filenames

extension String {
    
    private static let slugSafeCharacters = CharacterSet(charactersIn: "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-")
    
    public func convertedToSlug() -> String? {
        if let latin = self.applyingTransform(StringTransform("Any-Latin; Latin-ASCII; Lower;"), reverse: false) {
            let urlComponents = latin.components(separatedBy: String.slugSafeCharacters.inverted)
            let result = urlComponents.filter { $0 != "" }.joined(separator: "-")
            
            if result.count > 0 {
                return result
            }
        }
        
        return nil
    }
    
    /**
     validates username according to adduser.conf on Raspbiand
     
     
     from man adduser.conf on Raspbian:
     
     adduser enforce conformity to IEEE Std 1003.1-2001, which
     allows only the following characters to appear in group and
     user names: letters, digits, underscores, periods, at signs
     (@) and dashes. The name may not start with a dash. The "$"
     sign is allowed at the end of usernames (to conform to samba).
     
     */
    public func isValidLinuxUserName() -> Bool {
        
        let startsWithDash: Bool = starts(with: "-")
        
        let charactersInSelf = CharacterSet(charactersIn: self)
        let allCharactersAreValid = charactersInSelf.isSubset(of: CharacterSet.urlUserAllowed)
        
        return !self.isEmpty && allCharactersAreValid && !startsWithDash
    }
}
