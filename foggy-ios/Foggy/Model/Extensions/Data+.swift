//
//  Data+.swift
//  Foggy
//
//  Created by Stefan Wikner on 2021-01-13.
//  Copyright © 2021 Stefan Wikner. All rights reserved.
//

import Foundation
import CryptoKit

extension Data {
    var sha256SecureHash : Data {
        // SHA-256 was chosen since there is no hardware acceleration on the Raspberry PI SOC.
        // Comparing hashing performance on an iPhone 7 shows that is's also the fastest to compute:
        //     Filesize: 865.5 MB, SHA1: 1.054, SHA256: 0.553s, MD5: 2.318s
        let sha256Digest : SHA256Digest = CryptoKit.SHA256.hash(data: self)
        
        let ints : Array<UInt8> = sha256Digest.compactMap { UInt8($0) }
        
        var data = Data(capacity: SHA256.byteCount)
        data.append(contentsOf: ints)
        
        var data2 = Data(capacity: SHA256.byteCount)
        sha256Digest.withUnsafeBytes { data2.append(contentsOf: $0) }
        
        return data2
    }
}
