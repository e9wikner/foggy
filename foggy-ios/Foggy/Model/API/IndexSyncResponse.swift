//
//  RednasIdentifiersResponse.swift
//  Foggy
//
//  Created by Stefan Wikner on 2020-04-21.
//  Copyright © 2020 Stefan Wikner. All rights reserved.
//

import Foundation
import CocoaLumberjackSwift

struct IndexSyncResponse: Codable {
    let missing: Set<String>
    let remove: Set<String>
    
    init (from data: Data) {
        let jsonDecoder = JSONDecoder()
        do {
            let jsonDict = try jsonDecoder.decode(IndexSyncResponse.self, from: data)
            missing = jsonDict.missing
            remove = jsonDict.remove
            
        } catch {
            DDLogError(error.localizedDescription)
            missing = Set()
            remove = Set()
        }
    }
}
