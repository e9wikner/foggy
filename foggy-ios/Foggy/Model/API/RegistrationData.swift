//
//  Registration.swift
//  Foggy
//
//  Created by Stefan Wikner on 2021-01-14.
//  Copyright © 2021 Stefan Wikner. All rights reserved.
//

import Foundation

struct RegistrationData: Encodable {
    let user: String
    let device_name: String
}
