//
//  RegistrationResponse.swift
//  Foggy
//
//  Created by Stefan Wikner on 2021-02-02.
//  Copyright © 2021 Stefan Wikner. All rights reserved.
//

import Foundation

struct RegistrationResponse: Decodable {
    let password: String
    
}
