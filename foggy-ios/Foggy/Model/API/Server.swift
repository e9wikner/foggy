//
//  Backend.swift
//  Foggy
//
//  Created by Stefan Wikner on 2020-11-23.
//  Copyright © 2020 Stefan Wikner. All rights reserved.
//

import Foundation

struct Server {
    var hostname: String
    var ip: String
}
