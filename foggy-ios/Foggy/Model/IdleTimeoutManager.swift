//
//  BatteryManager.swift
//  Foggy
//
//  Created by Stefan Wikner on 2020-10-16.
//  Copyright © 2020 Stefan Wikner. All rights reserved.
//

import Foundation
import UIKit
import CocoaLumberjackSwift


class IdleTimeoutManager {

    func startManageOfIdleTimeout() {
        UIDevice.current.isBatteryMonitoringEnabled = true
        NotificationCenter.default.addObserver(self, selector: #selector(batteryStateDidChange), name: UIDevice.batteryStateDidChangeNotification, object: nil)
        setIdleTimeout()
    }
    
    func stopManageOfIdleTimeout() {
        NotificationCenter.default.removeObserver(self, name: UIDevice.batteryStateDidChangeNotification, object: nil)
        UIApplication.shared.isIdleTimerDisabled = false
        UIDevice.current.isBatteryMonitoringEnabled = false
    }
    
    fileprivate func setIdleTimeout() {
        if UIDevice.current.batteryState != UIDevice.BatteryState.unplugged {
            UIApplication.shared.isIdleTimerDisabled = true
            DDLogInfo("Idle timer disabled")
        } else {
            UIApplication.shared.isIdleTimerDisabled = false
            DDLogInfo("Idle timer enabled")
        }
    }
    
    @objc func batteryStateDidChange(_ notification: Notification) {
        setIdleTimeout()
    }
    
    deinit {
        stopManageOfIdleTimeout()
    }

}
