//
//  Index.swift
//  Foggy
//
//  Created by Stefan Wikner on 2020-04-23.
//  Copyright © 2020 Stefan Wikner. All rights reserved.
//

import Foundation
import SQLite
import Photos
import Combine
import CocoaLumberjackSwift

class Index : ObservableObject {
    
    typealias FoggyIdentifier = String
    
    let db: Connection
    let url: URL
    
    let tableLocalFiles = Table("local_files")
    let expressionIdentifier = Expression<String>("identifier")
    let expressionFilename = Expression<String>("filename")
    let expressionTimestamp = Expression<Double>("timestamp")
    let expressionTimestampModified = Expression<Double>("timestamp_modified")
    let expressionRemoved = Expression<Bool>("removed")
    let expressionNrOfBytes = Expression<Int?>("nr_of_bytes")
    let expressionSecureHash = Expression<Data?>("secure_hash")
    
    var nrOfOperations = 0
    var nrOfOperationsCompleted = 0
    
    // TODO: cancel on these two when needed
    var phContentEditingInputRequestIDs: Set<PHContentEditingInputRequestID> = Set()
    var phImageRequestIds: Set<PHImageRequestID> = Set()
    
    static let SCHEMA = """
    BEGIN TRANSACTION;
    CREATE TABLE IF NOT EXISTS local_files (
        identifier          TEXT    NOT NULL UNIQUE,
        filename            TEXT    NOT NULL,
        timestamp           REAL    NOT NULL,
        timestamp_modified  REAL    NOT NULL,
        removed             BOOL,
        nr_of_bytes         INTEGER,
        secure_hash         BLOB
    );
    COMMIT TRANSACTION;
    """
    static let defaultDatabaseUrl: URL = FileManager
        .default.urls(for: .documentDirectory, in: .userDomainMask).first!
        .appendingPathComponent("index.db")
    
    init(in url: URL = Index.defaultDatabaseUrl) throws {
        
        self.url = url
        let db_path = url.absoluteString
        DDLogInfo("Connecting to db in \(db_path)")
        
        do {
            self.db = try Connection(db_path)
            try db.execute(Index.SCHEMA)
        } catch {
            DDLogError(error)
            throw InitError.index
        }
        
    }
    
    var isAuthorized : Bool {
        switch PHPhotoLibrary.authorizationStatus() {
        case .authorized :
            return true
        default:
            return false
        }
    }
    
    // MARK: - lookups
    
    var allIdentifiers: Set<String> {
        var identifiers = Set<String>()
        if let statement = run("SELECT identifier FROM local_files") {
            for row in statement {
                identifiers.insert(row[0] as! String)
            }
        }
        return identifiers
    }
    
    var removedIdentifiers: Set<String> {
        var identifiers = Set<String>()
        if let statement = run("SELECT identifier FROM local_files WHERE removed=true") {
            for row in statement {
                identifiers.insert(row[0] as! String)
            }
        }
        return identifiers
    }
    
    func url(for identifier_: String) -> URL? {
        let query = tableLocalFiles.select(expressionFilename).filter(expressionIdentifier == identifier_)
        if let row = pluckOneRow(query) {
            
            let url = URL(string: row[expressionFilename])
            return url
            
        } else {
            DDLogError("Could not lookup url for \(identifier_)")
            return nil
        }
    }
    
    func needsIndexUpdate(_ asset: PHAsset) -> Bool {
        let query = tableLocalFiles.select(expressionTimestampModified).filter(expressionIdentifier == asset.identifier)
        if let row = pluckOneRow(query) {
            
            let assetInIndexTimestampModified = row[expressionTimestampModified]
            return asset.timestampModified != assetInIndexTimestampModified
            
        } else {
            DDLogError("Could not check if \(asset.identifier) was modified")
            return false
        }
    }
    
    // MARK: - Refresh
    
    func refresh() -> AnyPublisher<Int,NotAuthorizedError> {
    
        DDLogInfo("Refreshing local index")
        
        return Future() { promise in
            
            let fetchResult = PHAsset.fetchAssets(with: nil)
            
            guard self.isAuthorized else {
                let error = NotAuthorizedError.photos
                return promise(.failure(error))
            }
            
            let alreadyInsertedIdentifiers = self.allIdentifiers
            var identifiersInFetchResult: Set<String> = Set()
            
//            nrOfOperations = fetchResult.count
            
            for index in 0..<fetchResult.count {
                
//                nrOfOperationsCompleted = index
                
                let asset = fetchResult.object(at: index)
                let identifier: String = asset.identifier
                
                if alreadyInsertedIdentifiers.contains(identifier) {
                    
                    if self.needsIndexUpdate(asset) {
                        // also recovered files are getting a new modificationTimestamp as of iOS 13.1 ==> TODO: test that it's so
                        // TODO: pictures gets modified the first time the Photos app is opened after they are taken
                        self.assetWasModified(asset)
                    }
                    
                } else {
                    
                    self.insert(asset)
                    
                }
                
                identifiersInFetchResult.insert(identifier)
            }
            
            let identifiersNotRemoved = self.allIdentifiers.subtracting(self.removedIdentifiers)
            let identifiersToRemove = identifiersNotRemoved.subtracting(identifiersInFetchResult)
            
            self.identifiersWereRemoved(identifiersToRemove)
            
            DDLogInfo("Local index is refreshed")
            promise(.success(fetchResult.count))
        }
        .eraseToAnyPublisher()
    }
    
    // MARK: - CRUD
    
    
    private func insert(_ asset: PHAsset) {
        
        DDLogInfo("Inserting \(asset.identifier) modified: \(asset.modificationDate!.debugDescription)")
        var suffix = ".unknown"
        
        switch asset.mediaType {
            
        case PHAssetMediaType.image:
            suffix = ".jpg"
            
        case PHAssetMediaType.video:
            suffix = ".mov"
            
        default:
            DDLogError("Unsupported media type \(asset.mediaType) for \(asset.identifier)")
            
        }
        
        do {
            try db.run(tableLocalFiles.insert(
                expressionIdentifier <- asset.identifier,
                expressionFilename <- "\(asset.identifier)\(suffix)",
                expressionTimestamp <- asset.timestampCreated,
                expressionTimestampModified <- asset.timestampModified,
                expressionRemoved <- false,
                expressionNrOfBytes <- nil,
                expressionSecureHash <- nil
            ))
            
        } catch {
            DDLogInfo("insertion failed for \(asset.identifier): \(error)")
        }
        
    }
    
    private func assetWasModified(_ asset: PHAsset) {
        // nr_of_bytes is set to 0 to every time an update is made. The new value
        // should be set just before uploading to get the correct bytes count.
        
        DDLogInfo("Updating \(asset.identifier) modified: \(asset.modificationDate!.debugDescription)")
        
        // TODO: change url when updating a photo, same for video?
        do {
            
            let file = tableLocalFiles.filter(expressionIdentifier == asset.identifier)
            try db.run(file.update(
                expressionTimestampModified <- asset.timestampModified,
                expressionRemoved <- false,
                expressionNrOfBytes <- nil,
                expressionSecureHash <- nil
            ))
                        
        } catch {
            DDLogError("update failed: \(error)")
        }
    }
    
    /**
     Update nr of bytes and hash of exported asset
     */
    func assetWasExported(asset: PHAsset, data: Data) {
        let nrOfBytes = data.count
        let secureHash = data.sha256SecureHash
        assetWasExported(asset: asset, bytes: nrOfBytes, secureHash: secureHash)
    }
    
    private func assetWasExported(asset: PHAsset, bytes: Int, secureHash: Data) {
        
        let identifier = asset.identifier
        let hexDigest = secureHash.compactMap { String(format: "%02x", $0) }.joined()
        DDLogInfo("Updating \(identifier) bytes -> \(bytes), hash -> \(hexDigest)")
        
        do {
            
            let file = tableLocalFiles.filter(expressionIdentifier == identifier)
            try db.run(file.update(
                expressionNrOfBytes <- bytes,
                expressionSecureHash <- secureHash
            ))
                        
        } catch {
            DDLogError("update failed: \(error)")
        }
    }
    
    
    private func identifiersWereRemoved(_ identifiers: Set<String>) {
        
        if identifiers.isEmpty {
            return
        }
        
        DDLogInfo("Mark \(identifiers) as removed")
        
        let file = tableLocalFiles.filter(identifiers.contains(expressionIdentifier))
        do {
            try db.run(
                file.update(
                    expressionRemoved <- true
                )
            )
            
        } catch {
            DDLogError("remove failed: \(error)")
        }
        
    }
    
    // MARK: - helpers:
    
    private func run(_ query: String) -> Statement? {
        do {
            let statement = try db.run(query)
            return statement
        } catch {
            DDLogError(error.localizedDescription)
        }
        return nil
    }
    
    private func pluckOneRow(_ query: QueryType) -> Row? {
        do {
            if let row = try db.pluck(query) {
                return row
            }
        } catch {
            DDLogError("Could not pluck query \(query.asSQL()) in db")
        }
        return nil
    }
    
}
