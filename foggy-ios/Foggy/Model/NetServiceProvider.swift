//
//  NetServiceProvider.swift
//  Foggy
//
//  Created by Stefan Wikner on 2020-11-23.
//  Copyright © 2020 Stefan Wikner. All rights reserved.
//
import Foundation
import Combine
import CocoaLumberjackSwift


class NetServiceProvider : NSObject, NetServiceDelegate, NetServiceBrowserDelegate {

    private let browser = NetServiceBrowser()
    private let serviceType: String? = nil
    
    private var domainsWithServices : Set<String> = Set()
    private var foundServices : Set<NetService> = Set()

    @Published var searchResult : Server?
    
    override init() {
        super.init()
        browser.delegate = self
    }
    
    public func startSearch() {
        DDLogInfo("Starting search for registration domains")
        browser.searchForRegistrationDomains()
    }
    
    func abortSearch() {
        DDLogInfo("Search was aborted")
        browser.stop()
    }
    
    func netServiceBrowser(_ browser: NetServiceBrowser, didNotSearch errorDict: [String : NSNumber]) {
        // TODO: emit error
        DDLogInfo(#function)
    }
    
    func netServiceBrowser(_ browser: NetServiceBrowser, didRemove service: NetService, moreComing: Bool) {
        foundServices.remove(service)
    }
    
    func netServiceBrowser(_ browser: NetServiceBrowser, didFindDomain domainString: String, moreComing: Bool) {
        domainsWithServices.insert(domainString)
        
        if moreComing {
            return
        }
        browser.stop()
        
        for ds in domainsWithServices {
            DDLogInfo("Searching for service in domain '\(domainString)'")
            browser.searchForServices(ofType: "_foggy._tcp", inDomain: ds)
        }
    }
    
    func netServiceBrowser(_ browser: NetServiceBrowser, didFind service: NetService, moreComing: Bool) {
        DDLogInfo("Found \(service.type) service \(service.name):\(service.port)")
        
        foundServices.insert(service)
        if moreComing {
            return
        }
        browser.stop()
        
        for s in foundServices {
            s.delegate = self
            s.resolve(withTimeout: 0)
        }
        
    }
    
    func netServiceDidStop(_ sender: NetService) {
        self.foundServices.remove(sender)
        if searchResult != nil {
            // We only support one server at a time so once we have a resolved address we abort
            foundServices.removeAll()
        } else {
            DDLogError("Search for services timed out without a resolved IP")
        }
    }
    
    func netServiceBrowser(_ browser: NetServiceBrowser, didRemoveDomain domainString: String, moreComing: Bool) {
        DDLogInfo("Domain \(domainString) became unavailable")
        domainsWithServices.remove(domainString)
    }
    
    func netServiceBrowserDidStopSearch(_ browser: NetServiceBrowser) {
        DDLogInfo("Search was stopped")
    }
    
    func netServiceDidResolveAddress(_ sender: NetService) {
        
        // Find the IPV4 address
        if let serviceIp = resolveIPv4(addresses: sender.addresses!) {

            DDLogInfo("Found service IPV4: \(serviceIp)")
            searchResult = Server(hostname: sender.hostName!,
                                          ip: serviceIp)

        } else {
            DDLogError("Did not find IPV4 address")
        }
        
        sender.stop()
        
        // TODO: This is an example of decoding a text-record sent from the mdns service broadcast.
        //        if let data = sender.txtRecordData() {
        //            let dict = NetService.dictionary(fromTXTRecord: data)
        //            let value = String(data: dict["hello"]!, encoding: String.Encoding.utf8)
        //
        //            print("Text record (hello):", value!)
        //        }
        
    }
    
    func netService(_ sender: NetService, didNotResolve errorDict: [String : NSNumber]) {
        DDLogError("Did not resolve service")
    }

    
    // Find an IPv4 address from the service address data
    fileprivate func resolveIPv4(addresses: [Data]) -> String? {
        var result: String?
        
        for addr in addresses {
            let data = addr as NSData
            var storage = sockaddr_storage()
            data.getBytes(&storage, length: MemoryLayout<sockaddr_storage>.size)
            
            if Int32(storage.ss_family) == AF_INET {
                let addr4 = withUnsafePointer(to: &storage) {
                    $0.withMemoryRebound(to: sockaddr_in.self, capacity: 1) {
                        $0.pointee
                    }
                }
                
                if let ip = String(cString: inet_ntoa(addr4.sin_addr), encoding: .ascii) {
                    result = ip
                    break
                }
            }
        }
        
        return result
    }
}
