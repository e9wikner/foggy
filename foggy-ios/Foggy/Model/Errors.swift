//
//  Errors.swift
//  Foggy
//
//  Created by Stefan Wikner on 2020-12-02.
//  Copyright © 2020 Stefan Wikner. All rights reserved.
//

import Foundation

// TODO: refactor into something like in this viceo: https://youtu.be/dpmy-msRlCA
protocol FoggyError: Error {
    var localizedDescription: String { get }
}

enum InitError: FoggyError {
    case index
    case unknown
    
    var localizedDescription: String{
        switch self {
        case .index:
            return "Could not initialize local index database"
        case .unknown:
            return "Unknown init error"
        }
    }
}

enum NotAuthorizedError: FoggyError {
    case photos
    
    var localizedDescription: String {
        switch self {
        case .photos:
            return "Not authorized to access Photos, allow in Main Settings -> Foggy -> Photos"
        }
    }
}

enum ServerError: FoggyError {
    case config(description: String)
    case network(description: String)
    case networkTimeout
    
    var localizedDescription: String {
        switch self {
        case .config(let desc):
            return desc
        case .network(let desc):
            return desc
        case .networkTimeout:
            return "Network timeout"
        }
    }
}

enum HelloError: FoggyError {
    case config(description: String)
    case requestError(_ error: URLError)
    case registrationFailed(description: String)
    case notRegisteredDevice(deviceId: String)
    case unknown(description: String)
    
    var localizedDescription: String {
        switch self {
        case .config(let desc):
            return "Config error: \(desc)"
        case .requestError(let error):
            return "Communication error: \(error.localizedDescription)"
        case .registrationFailed(let desc):
            return desc
        case .notRegisteredDevice(let deviceId):
            return "\(deviceId) is not registered"
        case .unknown(let desc):
            return "Unknown error: \(desc)"
        }
    }
}

enum RequestAssetError: Error {
    case noAssetForIdentifier(_ identifier: Index.FoggyIdentifier)
}

enum ExportPHAssetDataError: FoggyError {
    case resultIsInCloud(description: String)
    case imageError(description: String)
    case couldNotGetAssetUrl
    case exportSessionFailed
    case failedReadingData
}
