//
//  Constants.swift
//  Foggy
//
//  Created by Stefan Wikner on 2020-04-21.
//  Copyright © 2020 Stefan Wikner. All rights reserved.
//

import SwiftUI
import CocoaLumberjackSwift

struct K {
    static let app = "foggy/api/v0.1"
    
    static let retry_timeout = 3.0
    
    static var deviceId: String? {
        if let urlSafeName = UIDevice.current.identifierForVendor?.uuidString {
            return urlSafeName
        } else {
            DDLogError("Should not happen! No device ID!")
            return nil
        }
    }
    static var deviceName: String {
        if let urlSafeName = UIDevice.current.name.convertedToSlug() {
            return urlSafeName
        } else {
            return UIDevice.current.model
        }
    }
    
    struct appStorageKeys {
        static let serverIp : String = "serverIp"
        static let serverName : String = "serverName"
        static let userName : String = "userName"
        static let password: String = "password"
    }
    
    struct colors {
        static let foreground = Color(red: 0.00, green: 0.68, blue: 0.71)
        static let background = Color(red: 0.17, green: 0.24, blue: 0.31)
    }
}
