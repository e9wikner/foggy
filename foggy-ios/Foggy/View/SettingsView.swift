//
//  SettingsView.swift
//  Foggy
//
//  Created by Stefan Wikner on 2020-11-12.
//  Copyright © 2020 Stefan Wikner. All rights reserved.
//

import SwiftUI

struct SettingsView: View {
    
    @StateObject var settingsViewModel : SettingsViewModel
    @StateObject var serverSearchViewModel: ServerSearchViewModel
    
    @Environment(\.presentationMode) var presentation
    
    fileprivate func enableDisableButtons(_ isEditing: Bool) {
        if isEditing {
            settingsViewModel.disableButtons = true
        } else {
            settingsViewModel.validateSettings()
        }
    }
    
    fileprivate func settingsTextField(label: String, placeholder: String, binding: Binding<String>) -> some View {
        return HStack {
            Text(label)
            TextField(placeholder,
                      text: binding,
                      onEditingChanged: enableDisableButtons)
                .autocapitalization(.none)
                .disableAutocorrection(true)
        }
    }
    
    var settingsForm: some View {
        
        Form {
            Section (header: Label("Server", systemImage: "server.rack")) {
                settingsTextField(label: "name",
                                  placeholder: "hostname",
                                  binding: $settingsViewModel.serverHostName)
                settingsTextField(label: "address",
                                  placeholder: "ip v4",
                                  binding: $settingsViewModel.serverAddress)
                settingsTextField(label: "user",
                                  placeholder: "login name",
                                  binding: $settingsViewModel.userName)
            }
        }.onAppear(perform: {
            settingsViewModel.validateSettings()
        })
    }
    
    fileprivate var searchButton: some View {
        Button(action: { serverSearchViewModel.findServerOnLocalNetwork() },
               label: { Label("Search", systemImage: "arrow.clockwise") })
    }
    
    fileprivate var doneButton: some View {
        Button(action: { presentation.wrappedValue.dismiss() },
               label: { Label("Done", systemImage: "checkmark") })
    }
    
    var body: some View {
        
        ZStack {
            
            VStack {
                settingsForm
                Spacer()
                VStack{
                    searchButton
                        .disabled(settingsViewModel.disableSearchButton)
                        .padding()
                    doneButton
                        .disabled(settingsViewModel.disableDoneButton)
                        .padding()
                }
            }
            .disabled(serverSearchViewModel.searchInProgress)
            .opacity(serverSearchViewModel.searchInProgress ? 0.2 : 1)
        }
        .sheet(
            isPresented: $serverSearchViewModel.searchInProgress,
            content: {
                ServerSearchView(serverSearchViewModel: serverSearchViewModel)
            })
        .onDisappear {
            serverSearchViewModel.cancel()
            settingsViewModel.register()
            // TODO: display registration error somehow, now I only get a log print, e.g. The operation couldn’t be completed. (Foggy.HelloError error 2.)
        }
    }
    
}

struct SettingsView_Previews: PreviewProvider {
    static var previews: some View {
        SettingsView(settingsViewModel: SettingsViewModel(),
                     serverSearchViewModel: ServerSearchViewModel())
        SettingsView(settingsViewModel: SettingsViewModel(),
                     serverSearchViewModel: ServerSearchViewModel())
            .preferredColorScheme(.dark)
            .previewDevice("iPad (8th generation)")
    }
}
