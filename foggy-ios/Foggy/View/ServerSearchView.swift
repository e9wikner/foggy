//
//  ServerSearchView.swift
//  Foggy
//
//  Created by Stefan Wikner on 2021-01-14.
//  Copyright © 2021 Stefan Wikner. All rights reserved.
//

import Foundation
import SwiftUI

struct ServerSearchView: View {
    
    @StateObject var serverSearchViewModel : ServerSearchViewModel
    
    var body: some View {
        return VStack {
            Spacer()
            ProgressView("Search in progress")
            Spacer()
            Button("Cancel") {
                serverSearchViewModel.cancel()
            }
        }
        .onDisappear(perform: {
            serverSearchViewModel.cancel()
        })
    }
    
}

struct ServerSearchView_Previews: PreviewProvider {
    static var previews: some View {
        ServerSearchView(serverSearchViewModel: ServerSearchViewModel())
        ServerSearchView(serverSearchViewModel: ServerSearchViewModel())
            .preferredColorScheme(.dark)
            .previewDevice("iPad (8th generation)")
    }
}
