//
//  SyncView.swift
//  Foggy
//
//  Created by Stefan Wikner on 2020-11-10.
//

import SwiftUI

struct SyncView: View {
    
    @ObservedObject var syncViewModel: SyncViewModel
    @ObservedObject var settingsViewModel: SettingsViewModel
    @ObservedObject var serverSearchViewModel: ServerSearchViewModel
    
    @State private var showAlert: Bool = false
    @State private var showSettings = false
    
    var settingsButton: some View {
        Button(action: {self.showSettings.toggle()}, label: {
            Label("settings", systemImage: "gearshape.fill")
        })
        .foregroundColor(K.colors.foreground)
        .padding(/*@START_MENU_TOKEN@*/.all/*@END_MENU_TOKEN@*/)
    }
    
    var errorAlert: Alert {
        Alert(title: Text("Error:"),
              message: Text(syncViewModel.syncError?.localizedDescription ?? "The reason is unknown"),
              dismissButton: .default(Text("OK"),
                                      action: { syncViewModel.showError.toggle() })
        )
    }
    
    var body: some View {
        ZStack {
            K.colors.background.edgesIgnoringSafeArea(.all)
            
            VStack {
                
                Spacer()
                HStack{
                    Spacer()
                    ProgressView(value: syncViewModel.syncProgress)
                        .opacity(syncViewModel.syncProgress > 0.0 ? 1 : 0)
                        .padding()
                    Spacer()
                }
                Text(syncViewModel.syncState.rawValue)
                    .foregroundColor(K.colors.foreground)
                
                Spacer()
                
                Button(action: {
                    syncViewModel.syncFiles()
                })
                {
                    VStack {
                        Image("sync")
                            .resizable()
                            .aspectRatio(1, contentMode: /*@START_MENU_TOKEN@*/.fill/*@END_MENU_TOKEN@*/)
                        Text("sync now")
                            .foregroundColor(K.colors.foreground)
                    }
                    .frame(width: 100, height: /*@START_MENU_TOKEN@*/100.0/*@END_MENU_TOKEN@*/)
                    
                }
                .disabled(syncViewModel.syncState.disableSync)
                .opacity(syncViewModel.syncState.disableSync ? 0.1 : 1)
                Spacer()
                settingsButton
            }
            .alert(isPresented: $syncViewModel.showError,
                   content: {errorAlert})
            .sheet(isPresented: $showSettings,
                   content: {
                    SettingsView(settingsViewModel: settingsViewModel,
                                 serverSearchViewModel: serverSearchViewModel)
                        .onDisappear() {
                            syncViewModel.connect()
                        }
                   })
        }
        .onReceive(NotificationCenter.default.publisher(for: UIApplication.willEnterForegroundNotification)) { _ in
            syncViewModel.finishBackgroundTask(success: false)
            syncViewModel.refresh()
        }
        // TODO:
//        .onReceive(NotificationCenter.default.publisher(for: UIApplication.willResignActiveNotification)) { _ in
//            syncViewModel.pause()
//        }
        .onReceive(NotificationCenter.default.publisher(for: UIApplication.didEnterBackgroundNotification)) { _ in
            syncViewModel.abort()
            syncViewModel.scheduleBackgroundRefresh()
        }
    }
}

struct SyncView_Previews: PreviewProvider {
    static var previews: some View {
        SyncView(syncViewModel: SyncViewModel(),
                 settingsViewModel: SettingsViewModel(),
                 serverSearchViewModel: ServerSearchViewModel())
        SyncView(syncViewModel: SyncViewModel(),
                 settingsViewModel: SettingsViewModel(),
                 serverSearchViewModel: ServerSearchViewModel())
            .preferredColorScheme(.dark)
            .previewDevice("iPad (8th generation)")
    }
}
