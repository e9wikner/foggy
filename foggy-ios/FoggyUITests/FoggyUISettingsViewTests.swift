//
//  FoggyUITests.swift
//  FoggyUITests
//
//  Created by Stefan Wikner on 2021-01-26.
//  Copyright © 2021 Stefan Wikner. All rights reserved.
//

import XCTest

class FoggyUISettingsViewTests: XCTestCase {
    
    var app: XCUIApplication!
    
    override func setUpWithError() throws {
        
        continueAfterFailure = false
        
        app = XCUIApplication()
        app.launchArguments = ["--clear-settings"]
        app.launch()
        
        addUIInterruptionMonitor(withDescription: "Dismiss errors") { (element) -> Bool in
            if element.title == "Error:" {
                let okButton = element.buttons["OK"]
                okButton.tap()
                return true
            }
            return false
        }
        
        app.buttons["settings"].tap()
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testButtonsWithEmptySettings() throws {

        XCTAssertTrue(app.buttons["Search"].isEnabled)
        XCTAssertTrue(app.buttons["Done"].isEnabled)

    }
    
    func testButtonsWhenInvalidUsername() throws {
        
        let userNameTextField = app.tables/*@START_MENU_TOKEN@*/.textFields["login name"]/*[[".cells[\"user\"].textFields[\"login name\"]",".textFields[\"login name\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        userNameTextField.waitForExistenceAndTapOtherwiseFail()
        userNameTextField.typeText("arne@")
        
        app.keyboards.buttons["Return"].tap()
        
        XCTAssertTrue(app.buttons["Search"].isEnabled)
        XCTAssertTrue(app.buttons["Done"].isEnabled)  // TODO: should be disabled
        
    }
}
