//
//  FoggyUITests.swift
//  FoggyUITests
//
//  Created by Stefan Wikner on 2020-03-27.
//  Copyright © 2020 Stefan Wikner. All rights reserved.
//

import XCTest

class FoggyInitialLaunchUITests: XCTestCase {
    // The simulation device needs to be erased and the server should be cleaned before launching the testcase
    var app: XCUIApplication!
    
    override func setUpWithError() throws {
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
        app = XCUIApplication()
//        app.launchArguments = ["--temp-index"]
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    fileprivate func assertElementValueNotEmptyString(_ textField: XCUIElement) {
        let value = textField.value as! String
        XCTAssertNotEqual(value, "")
    }
    
    fileprivate func initPhotosApp() {
        let photosApp = activatePhotosApp()
        
        // TODO: these two taps should be handled in some kind of eventhandler to speed up results
        let continueButton = photosApp.buttons["Continue"]
        if continueButton.waitForExistence(timeout: 2) {
            // The first time the app launches this button have to be tapped
            continueButton.tap()
        }
        
        let photosGridView = photosApp.collectionViews["PhotosGridView"]
        photosGridView.cells.firstMatch.tap()
        
        let gotItButton = photosApp.buttons["Got It"]
        if gotItButton.waitForExistence(timeout: 2) {
            // The first time the app launches this button have to be tapped
            gotItButton.tap()
        }
        
        photosApp.buttons["All Photos"].waitForExistenceAndTapOtherwiseFail()
    }
    
    fileprivate func activatePhotosApp() -> XCUIApplication {
        let photosApp = XCUIApplication(bundleIdentifier: "com.apple.mobileslideshow")
        photosApp.activate()
        return photosApp
    }
    
    fileprivate func assertSync() {
        let syncButton = app.buttons["sync now"]
        let syncStateText = app.staticTexts.firstMatch
        
        app.activate()
        
        expectation(for: NSPredicate(format: "isEnabled == true"),
                    evaluatedWith: syncButton,
                    handler: nil)
        waitForExpectations(timeout: 10.0,
                            handler: nil)
            
        XCTAssertTrue(syncButton.isEnabled)
        syncButton.tap()
        
        expectation(for: NSPredicate(format: "label == 'Content in sync'"),
                    evaluatedWith: syncStateText,
                    handler: nil)
        waitForExpectations(timeout: 30.0,
                            handler: nil)
        
        XCTAssertFalse(syncButton.isEnabled)
    }
    
    func test_00_FirstAppLaunch() throws {
        
        initPhotosApp()
        
        app.resetAuthorizationStatus(for: .photos)
        app.launch()
        
        let springboardApp = XCUIApplication(bundleIdentifier: "com.apple.springboard")
        let alert = springboardApp.alerts["“Foggy” Would Like to Access Your Photos"]
        
        let allowAccessButton = alert.buttons["Allow Access to All Photos"]
        allowAccessButton.waitForExistenceAndTapOtherwiseFail()
        
        let errorAlert = app.alerts["Error:"]
        let okButton = errorAlert.buttons["OK"]
        okButton.waitForExistenceAndTapOtherwiseFail()
        
        app.buttons["settings"].waitForExistenceAndTapOtherwiseFail()
        app.buttons["Search"].waitForExistenceAndTapOtherwiseFail()
        
        let tablesQuery = app.tables
        tablesQuery.element.waitForExistenceOtherwiseFail()
        
        assertElementValueNotEmptyString(tablesQuery.textFields["hostname"])
        assertElementValueNotEmptyString(tablesQuery.textFields["ip v4"])
        
        tablesQuery.element.swipeDown()
        
        assertSync()
    }
    
    fileprivate func activatePhotosAppAndClickFirstPhoto() -> XCUIApplication {
        let photosApp = activatePhotosApp()
        let photosGridView = photosApp.collectionViews["PhotosGridView"]
        let firstPhoto = photosGridView.cells.firstMatch
        firstPhoto.waitForExistenceAndTapOtherwiseFail()
        return photosApp
    }

    func test_01_SyncPhotoDuplication() {
        let photosApp = activatePhotosAppAndClickFirstPhoto()
        
        photosApp.buttons["Share"].waitForExistenceAndTapOtherwiseFail()
        photosApp.buttons["Duplicate"].waitForExistenceAndTapOtherwiseFail()
        
        // To make sure that the changes are saved before switching app
        photosApp.buttons["All Photos"].waitForExistenceAndTapOtherwiseFail()

        assertSync()
    }
    
    func test_02_SyncPhotoEdit() {
        let photosApp = activatePhotosAppAndClickFirstPhoto()
        
        photosApp.buttons["Edit"].waitForExistenceAndTapOtherwiseFail()
        photosApp.buttons["Plugins"].waitForExistenceAndTapOtherwiseFail()
        photosApp.buttons["Markup"].waitForExistenceAndTapOtherwiseFail()
        
        let page = photosApp.otherElements["Page 1"]
        page.waitForExistenceOtherwiseFail()
        page.swipeRight()
        
        photosApp.buttons["Done"].tap()
        photosApp/*@START_MENU_TOKEN@*/.staticTexts["Done"]/*[[".buttons[\"Done\"].staticTexts[\"Done\"]",".staticTexts[\"Done\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.waitForExistenceAndTapOtherwiseFail()
        
        // To make sure that the changes are saved before switching app
        photosApp.buttons["All Photos"].waitForExistenceAndTapOtherwiseFail()
        
        assertSync()
    }
    
    func test_03_SyncPhotoRemoval() {
        let photosApp = activatePhotosAppAndClickFirstPhoto()
        
        photosApp.buttons["Delete"].waitForExistenceAndTapOtherwiseFail()
        photosApp.buttons["Delete Photo"].waitForExistenceAndTapOtherwiseFail()
        
        // To make sure that the changes are saved before switching app
        photosApp.buttons["All Photos"].waitForExistenceAndTapOtherwiseFail()
        assertSync()
    }

}

extension XCUIElement {
    
    func waitForExistenceOtherwiseFail(timeout: TimeInterval = 2) {
        if !waitForExistence(timeout: timeout) {
            XCTFail()
        }
    }
    
    func waitForExistenceAndTapOtherwiseFail(timeout: TimeInterval = 2) {
        if waitForExistence(timeout: timeout) {
            tap()
        } else {
            XCTFail()
        }
    }
}
