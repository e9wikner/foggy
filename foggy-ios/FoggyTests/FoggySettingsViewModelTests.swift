//
//  FoggySettingsViewModelTests.swift
//  FoggyTests
//
//  Created by Stefan Wikner on 2021-01-29.
//  Copyright © 2021 Stefan Wikner. All rights reserved.
//

import XCTest
@testable import Foggy


class FoggySettingsViewModelTests: XCTestCase {
    
    var settingsViewModel: SettingsViewModel!
    
    override func setUpWithError() throws {
        settingsViewModel = SettingsViewModel()
        settingsViewModel.serverAddress = "0.0.0.0"
        settingsViewModel.userName = ""
        settingsViewModel.validateSettings()
    }
    
    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    // Should be tested in a unit test but that shit does not work for me
    func testString_isValidLinuxUserName() throws {
        XCTAssertTrue("arne".isValidLinuxUserName())
        XCTAssertFalse("".isValidLinuxUserName())
        XCTAssertFalse("arne#".isValidLinuxUserName())
        XCTAssertFalse("arne@".isValidLinuxUserName())
    }
    
    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
