//
//  FoggyTests.swift
//  FoggyTests
//
//  Created by Stefan Wikner on 2020-03-27.
//  Copyright © 2020 Stefan Wikner. All rights reserved.
//

import XCTest
@testable import Foggy

/** Mockup dispatchqueue that runs async code synchronous
 */

final class DispatchQueueMock: DispatchQueueType {
    func async(execute work: @escaping @convention(block) () -> Void) {
        work()
    }
}

class FoggySyncViewModelTests: XCTestCase {

    var syncViewModel: SyncViewModel!
    
    override func setUpWithError() throws {
        syncViewModel = SyncViewModel(mainDispatchQueue: DispatchQueueMock(), inhibitLaunch: true)
        syncViewModel.serverManager.ip = "0.0.0.0"
        syncViewModel.serverManager.name = "localhost"
        syncViewModel.serverManager.userName = NSUserName()
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testViewModelInit() throws {
        XCTAssertEqual(syncViewModel.syncState, .launching)
    }
}
