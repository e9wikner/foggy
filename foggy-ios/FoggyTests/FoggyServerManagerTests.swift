//
//  FoggyServerManagerTests.swift
//  FoggyTests
//
//  Created by Stefan Wikner on 2021-02-02.
//  Copyright © 2021 Stefan Wikner. All rights reserved.
//

import XCTest
import Combine
@testable import Foggy

class FoggyServerManagerTests: XCTestCase {

    var serverManager: ServerManager!
    var cancellables = Set<AnyCancellable>()
    
    override func setUpWithError() throws {
        serverManager = ServerManager()
        serverManager.ip = "0.0.0.0"
        serverManager.name = "localhost"
        serverManager.userName = ""
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testRegisterDevice() throws {
        let promise = expectation(description: "Server responds")
        var responseError: HelloError?
        var registrationResponse: RegistrationResponse?
        
        serverManager.registerDevice()
            .sink(
                receiveCompletion: { value in
                    switch value {
                    case .failure(let error):
                        responseError = error
                        promise.fulfill()
                    case .finished:
                        promise.fulfill()
                        break
                    }
                },receiveValue: { registrationResponse = $0 })
            .store(in: &cancellables)
            
        wait(for: [promise], timeout: 2)
        
        XCTAssertNotNil(responseError)
    }

    func testSayHello() throws {
        let promise = expectation(description: "Server responds")
        let expectedError = HelloError.notRegisteredDevice(deviceId: K.deviceId!)
        var responseError: HelloError?
        var responseValue: Data?
        
        serverManager.sayHello()
            .sink(
                receiveCompletion: { value in
                    switch value {
                    case .failure(let error):
                        promise.fulfill()
                        responseError = error
                        break
                    case .finished:
                        promise.fulfill()
                        break
                    }
                },receiveValue: { responseValue = $0 })
            .store(in: &cancellables)
            

        wait(for: [promise], timeout: 4)
        
        XCTAssertNotNil(responseError)
        XCTAssertNil(responseValue)
        // TODO: XCTAssertEqual(responseError!.localizedDescription, expectedError.localizedDescription)
        
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
