//
//  testswiftuiApp.swift
//  testswiftui
//
//  Created by Stefan Wikner on 2020-11-13.
//

import SwiftUI
import CocoaLumberjackSwift
import BackgroundTasks

// TODO:
//      - After install there is no server discovered in settings causing an error
//          => Show settings when there is not server configured

@main
struct testswiftuiApp: App {
    
    init() {
        
        DDLog.add(DDOSLogger.sharedInstance) // Uses os_log
        
        let fileLogger: DDFileLogger = DDFileLogger() // File Logger
        fileLogger.rollingFrequency = 60 * 60 * 24 // 24 hours
        fileLogger.logFileManager.maximumNumberOfLogFiles = 7
        DDLog.add(fileLogger)
        DDLogInfo("App init done")
    }
    var body: some Scene {
        let syncViewModel = SyncViewModel()
        let settingsViewModel = SettingsViewModel()
        let serverSearchViewModel = ServerSearchViewModel()
        BGTaskScheduler.shared.register(forTaskWithIdentifier: "se.stefanwikner.Foggy.syncIndex", using: nil) { task in
            syncViewModel.handleIndexSync(task: task as! BGProcessingTask)
        }
        #if DEBUG
        syncViewModel.setDebugMode()
        settingsViewModel.setDebugMode()
        #endif
        return WindowGroup {
            SyncView(syncViewModel: syncViewModel,
                     settingsViewModel: settingsViewModel,
                     serverSearchViewModel: serverSearchViewModel)
        }
    }
}
