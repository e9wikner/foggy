#!/usr/bin/python3
"""Install required packages on system"""
import argparse
import subprocess
from pathlib import Path

INSTALL_APT = [
    "apt-get",
    "install",
    "--assume-yes",
    "python3-venv",
    "acl",
]
INSTALL_PACMAN = ["pacman", "-S", "--noconfirm", "--noprogressbar", "--needed"]
BASEDIR = Path(__file__).parent.absolute()


def detect_install_command():
    try:
        subprocess.run(["apt-get", "--version"], check=True, capture_output=True)
    except FileNotFoundError:
        subprocess.run(["pacman", "--version"], check=True, capture_output=True)
        return INSTALL_PACMAN
    else:
        return INSTALL_APT


def run(args, check=True):
    """Run COMMAND and return lines"""
    if isinstance(args, str):
        args = args.split()
    print(" ".join(args))
    subprocess.run(args, check=check)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("user", help="User that the service will run as")
    args = parser.parse_args()

    # Install packages
    run(detect_install_command())

    # Install this
    venv_dir = BASEDIR.parent / ".foggy" / "venv"
    venv_pip = venv_dir / "bin" / "pip"
    run(f"python3 -m venv {venv_dir}")
    run(f"{venv_pip} install --editable {BASEDIR / 'backend'}")
    run(f"ln -s {venv_dir / 'bin' / 'foggy'} /usr/bin/foggy")
    run("chmod +x /usr/bin/foggy")

    # Install service
    service_path = Path("etc") / "systemd" / "system" / "foggy@.service"
    # run(["ln", "-s", str(BASEDIR / service_path), str("/" / service_path)])
    run(f"systemctl link {service_path}")
    run(f"systemctl reenable foggy@{args.user}")
    run(f"systemctl restart foggy@{args.user}")


if __name__ == "__main__":
    main()
