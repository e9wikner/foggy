from zeroconf import Zeroconf, IPVersion

from foggy import mdns


def test_broadcast_service():
    zeroconf = Zeroconf(ip_version=IPVersion.V4Only)

    with mdns.broadcast_service():
        info = zeroconf.get_service_info(type_=mdns.SERVICEINFO.type, name=mdns.SERVICEINFO.name)
        zeroconf.close()
        assert info.port == 21210
