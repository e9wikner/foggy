"""Test module for the foggy.filedb module"""
from concurrent.futures import ThreadPoolExecutor, wait
import random

import pytest

import foggy.filedb as lib
import foggy.utils


def test_random_identifier(local_identifier):
    assert isinstance(local_identifier, str)


def test_UserHomeDoesNotExist():
    with pytest.raises(lib.UserHomeDoesNotExist):
        lib.FileDB.register(user="I_do_not_exist", device_name="name", device_id="id")


@pytest.mark.parametrize("name", ["", "/bin/bash", "/", "../../bin"])
def test_malicous_user_home(name):
    with pytest.raises(lib.MaliciousUserHome):
        lib.FileDB.check_user_home(name)


def test_register(clean_filedb, device_id):
    root = lib.FileDB.root_for_device(device_id)
    assert root == clean_filedb.root_path


def test_check_device_name():
    with pytest.raises(lib.DeviceIdException) as exc:
        lib.FileDB.check_device_name("")
        assert "empty" in str(exc)


def test_DeviceNotRegisteredError():
    with pytest.raises(lib.DeviceNotRegisteredError):
        lib.FileDB.root_for_device("not_registered_device")


def test_db_exists(clean_filedb):
    assert clean_filedb.db_path.exists()


def test_path_with_filename_collision_counter(filedb, local_target_filepath):
    filename = filedb.get_insertable_path(local_target_filepath)
    suffix = local_target_filepath.suffix
    expected_filename = str(local_target_filepath).replace(suffix, f"_01{suffix}")
    assert str(filename) == expected_filename


def test_new_safe_path(clean_filedb, local_target_filepath, local_timestamp):
    result = clean_filedb.convert_to_path_that_can_be_inserted(
        timestamp=local_timestamp, suffix=local_target_filepath.suffix
    )
    assert local_target_filepath == result


def test_mark_as_removed(filedb, local_identifier):
    filedb.mark_as_removed(local_identifier)
    row = filedb.lookup_identifier(identifier=local_identifier)
    assert row["removed"]


def test_lookup_identifier(filedb, local_identifier):
    item = filedb.lookup_identifier(local_identifier)
    assert local_identifier == item["identifier"]


def test_lookup_filename(filedb, local_target_filepath):
    item = filedb.lookup_filename(local_target_filepath)
    assert str(local_target_filepath) == item["filename"]


def test_delete(filedb, local_identifier):
    filedb.delete(local_identifier)
    with pytest.raises(lib.ItemNotFound):
        filedb.lookup_identifier(local_identifier)


def test_delete_does_not_empty_entire_db(tmp_path, filedb, local_timestamp):
    all_items = filedb.lookup_all()
    assert 4 == len(all_items)

    another_identifier = foggy.utils.random_identifier()
    another_item_path = tmp_path / "file2"
    another_item_path.touch()
    filedb.insert(
        identifier=another_identifier, suffix=".jpg", timestamp=local_timestamp
    )

    all_items = filedb.lookup_all()
    assert 5 == len(all_items)

    filedb.delete(another_identifier)
    with pytest.raises(lib.ItemNotFound):
        filedb.lookup_identifier(another_identifier)

    all_items = filedb.lookup_all()
    assert 4 == len(all_items)


def test_db_not_locked_after_identifier_collision(
    filedb, local_identifier, local_suffix, local_timestamp
):
    with pytest.raises(lib.ItemAlreadyInserted):
        filedb.insert(
            identifier=local_identifier, suffix=local_suffix, timestamp=local_timestamp
        )
    item = filedb.lookup_identifier(local_identifier)
    assert item


def test_multiple_threads_initialize_db(clean_filedb, device_id, local_target_filepath):
    def init_db_and_insert_item(thread_nr):
        identifier = f"identifier_from_thread_{thread_nr}"
        timestamp = random.random() * 1000
        clean_filedb = lib.FileDB(device_id=device_id)
        clean_filedb.create_files_table()
        clean_filedb.insert(
            identifier=identifier,
            suffix=local_target_filepath.suffix,
            timestamp=timestamp,
        )

    with ThreadPoolExecutor() as executor:
        futures = [executor.submit(init_db_and_insert_item, i) for i in range(10)]
    complete, not_complete = wait(futures, timeout=1)
    assert not not_complete

    exceptions = [f.exception() for f in complete]
    assert not any(exceptions)


def test_add_item_with_same_timestamp(filedb, local_target_filepath, local_timestamp):
    """it's possible that e.g. two different images has the same timestamp"""
    new_path = filedb.convert_to_path_that_can_be_inserted(
        timestamp=local_timestamp, suffix=local_target_filepath.suffix
    )
    assert new_path != local_target_filepath


def test_sync_remote(remote_identifier, filedb_with_synced_remote):
    assert filedb_with_synced_remote.lookup_identifier(remote_identifier, remote=True)


def test_sync_remote_can_be_done_again(filedb_with_synced_remote, filedb_on_remote):
    filedb_with_synced_remote.sync_remote_index(
        filedb_on_remote.db_path.open(mode="rb")
    )
