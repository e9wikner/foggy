"""Integration test module

In this test we have uploaded a remote database and have a partly completed files
upload. Then the connection is dropped and when we sync again there have been some
changes in the remote db.

So this test should check that we account for all missing, updated and removed
identifiers:
- 1 file to be uploaded
- 4 files were removed before upload
- 1 file has been modified since upload
- 1 file has been removed since upload

==> 2 missing files
==> 1 file to remove

"""


def test_update_identifiers(filedb_with_synced_remote, identifiers_to_be_updated):
    assert (
        filedb_with_synced_remote.identifiers_to_be_updated == identifiers_to_be_updated
    )


def test_remove_identifiers(filedb_with_synced_remote, identifiers_to_be_removed):
    assert (
        filedb_with_synced_remote.identifiers_to_be_removed == identifiers_to_be_removed
    )


def test_update_from_remote_db(
    filedb_with_synced_remote, identifiers_to_be_updated, tmp_path
):
    file = tmp_path / "file"
    file.touch()
    file.write_text("123")
    identifier = identifiers_to_be_updated.pop()
    filedb_with_synced_remote.sync_remote_file(
        identifier=identifier, file_like_object=file.open(mode="rb")
    )

    row_in_remote = filedb_with_synced_remote.lookup_identifier(
        identifier=identifier, remote=True
    )
    row_in_local = filedb_with_synced_remote.lookup_identifier(
        identifier=identifier, remote=False
    )

    assert row_in_local != row_in_remote
    assert ":" not in row_in_local["filename"]
    assert row_in_local["nr_of_bytes"] == 3
    assert row_in_local["secure_hash"] is not None
