"""Configuration module for foggy tests.

Each fixture is version to denote each update:
    - Version 0: db is initiated
    - Version 1: item is inserted
    - Version 2: item is updated
    - Version 3: item is deleted
"""
import json
from datetime import datetime

import falcon.testing
import pytest

import foggy.api.app
import foggy.utils


API_URL = "/foggy/api/v0.1"
TIMESTAMP = datetime(2018, 1, 7, 15, 18, 31).timestamp()


@pytest.fixture
def device_id():
    return "test_client"


@pytest.fixture
def api_url():
    return API_URL


@pytest.fixture
def registration_json_dict(user, device_name):
    return {"user": user,
            "device_name": device_name}


@pytest.fixture
def client_clean(device_id, registration_json_dict):
    app = foggy.api.app.get_app()
    client = falcon.testing.TestClient(app)
    response = client.simulate_put(f"{API_URL}/hello/{device_id}",
                                   json=registration_json_dict)
    assert falcon.HTTP_200 == response.status
    return client


@pytest.fixture
def client_with_synced_filedb_on_remote(
    client_clean,
    api_url,
    device_id,
    filedb_on_remote,
    identifiers_to_be_added,
    identifiers_to_be_updated,
    remote_identifier,
):
    url = f"{api_url}/index/{device_id}"
    response = client_clean.simulate_put(
        url,
        body=filedb_on_remote.db_path.read_bytes(),
        headers={"Content-Type": "application/octet-stream"},
    )
    assert 200 == response.status_code
    expected_json = {
        "missing": sorted(identifiers_to_be_updated | identifiers_to_be_added),
        "remove": [],
    }
    assert response.json == expected_json
    assert [remote_identifier] not in response.json["missing"]
    return client_clean


@pytest.fixture
def client_in_sync(
    client_with_synced_filedb_on_remote,
    api_url,
    device_id,
    remote_identifier,
    remote_filepath,
):
    url = f"{api_url}/files/{device_id}/{remote_identifier}"
    response = client_with_synced_filedb_on_remote.simulate_put(
        url,
        body=remote_filepath.read_bytes(),
        headers={"Content-Type": "application/octet-stream"},
    )
    assert 200 == response.status_code
    return client_with_synced_filedb_on_remote
