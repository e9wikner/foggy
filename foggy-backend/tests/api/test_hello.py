import falcon


def test_404_DeviceNotRegisteredError(client_clean, api_url):
    url = f"{api_url}/hello/notregistereddevice"
    response = client_clean.simulate_get(url)
    assert response.status == falcon.HTTP_404


def test_hello(client_clean, api_url, device_id, user, device_name):
    url = f"{api_url}/hello/{device_id}"
    response = client_clean.simulate_get(url)
    assert response.status == falcon.HTTP_200
